/* tslint:disable:no-unused-variable */

import { Calculator } from './programm';

describe('Calculator', () => {
  let calculator: Calculator;
  beforeEach(() => {
   calculator = new Calculator();
  });

  it('should multiply values',() => {
    expect(calculator.multiply(2, 3)).toBe(6);
  });

  it('should multiply zeros',() => {
    expect(calculator.multiply(0, 0)).toBe(0);
  });

 
});
